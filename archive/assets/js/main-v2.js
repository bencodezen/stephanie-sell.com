$(document).ready(function() {
  $('#hamburger').on('click', function() {
    var $windowWidth = $(window).width();
    $('#mobile-nav').width($windowWidth);
    $('#mobile-nav ul').width($windowWidth);
    $('#mobile-nav').slideToggle();
  });

  $('#back-to-top').on('click', function (e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: "0"
    }, 900);
  });
});